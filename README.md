# Ansible base role

## Variables

Variable that start with an underscore (_) are defined in vars/main.yml all other vars are defined in defaults/main.yml of supposed to be defined in your inventory.

For practical usage you can find all the external definable variables in defaults/main.yml as a comment.  

The variables {{ desktop_production }} & {{ desktop_join_domain }} are supposed to be set in you inventory but a default value is set in defaults/main.yml

```
desktop_production: false
desktop_join_domain: false
```

Vars {{ domain.name }} & {{ domain.user }} & {{ domain.user_pw }} are supposed to be set in group_vars/all/main.yml of the inventory


Set time related settings:

```
timezone: Europe/Amsterdam # Is the default set in vars/main.yml (Check available entries with timedatectl list-timezones)
```

## usage

Add the following to a playbook to user this base role:

```
- name: Install base
  hosts: all
  gather_facts: true
  become: true
  tasks:
    - name: Apply base role
      include_role:
        name: base
        apply:
          tags: base
      tags: always
```

And add the following to your requirements.yml

```
- src: https://gitlab.com/naturalis/lib/ansible/ansible-role-ubuntu-desktop
  scm: git
  name: base
```
